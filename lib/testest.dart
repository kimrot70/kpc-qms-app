
/*

import 'dart:collection';
import 'dart:convert';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models.dart';

class QueuesPage extends StatefulWidget {
   final String text;
 QueuesPage({Key key, @required this.text}) : super(key: key);

  @override
  _QueuesPageState createState() => _QueuesPageState();
}

class _QueuesPageState extends State<QueuesPage> {
  @override
  Widget build(BuildContext context) {

    
     final debouncer = Debouncer(milliseconds: 1000);
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Queues'),
      ),
      body: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(15.0),
              hintText: 'location',
            ),
            onChanged: (string) {
              _debouncer.run(() {
                setState(() {
                   = depots
                      .where((u) => (u.code
                              .toLowerCase()
                              .contains(string.toLowerCase()) ||
                          u.location.toLowerCase().contains(string.toLowerCase())))
                      .toList();
                });
              });
            },
          ),
        ]
      )
    );

  }

  Widget futureWidget() {
    
 
    List<Queue> list = List();
  
    List<Queue>  items = List();

    

    String url ="https://qmseldoret.kpc.co.ke/handheld/broadqueue/${widget.text}";
    return FutureBuilder(
      future: http.get(url),
      
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var queues = json.decode(snapshot.data.body);
          var queue = queues['content'] as List;
          list = queue.map<Queue>((json)=>Queue.fromJson(json)).toList();
          items.addAll(list);
                    
          
              return _buildqueues(context, items);          
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}


  ListView _buildqueues(BuildContext context, List queues){
    return ListView.builder( 
      itemCount: queues.length,
      
      itemBuilder: (contex, index){
        
        return Container(
          
          child: Center(
            
            child: Card(
              color: HexColor(queues[index].color),
              elevation: 10,
              child: ListTile(
                isThreeLine: true,
                
                leading: Icon(Icons.queue, color: Colors.black,),               
                title: Text("Queue Number: ${queues[index].queueNumber}", 
                          style: TextStyle(
                          
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(4),
                    ),

                    Text(queues[index].checkpoint,
                          
                          style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold,color: Colors.black)),
                    Text(queues[index].vehicleNumber,
                          
                          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,color: Colors.black),),
                 Text(queues[index].omc,
                          
                          style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.bold,),),
                 Text(queues[index].driverName,
                          
                          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),),
                 Text(formatDate(DateTime.parse(queues[index].time), [d, '-', M, '-', yyyy, ', ', HH, ':', nn]),
                          
                          style: TextStyle(fontSize:14.0, fontWeight: FontWeight.bold,),)
                  ],
                ),
                
                contentPadding: EdgeInsets.all(10),  ),
            ),
          ),
        );
      }
    );
  }
*/